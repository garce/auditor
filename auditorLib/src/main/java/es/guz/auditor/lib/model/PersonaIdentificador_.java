package es.guz.auditor.lib.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-11-19T20:43:44.012+0100")
@StaticMetamodel(PersonaIdentificador.class)
public class PersonaIdentificador_ {
	public static volatile SingularAttribute<PersonaIdentificador, String> id;
	public static volatile SingularAttribute<PersonaIdentificador, String> identificador;
	public static volatile SingularAttribute<PersonaIdentificador, String> tipo;
	public static volatile SingularAttribute<PersonaIdentificador, Persona> persona;
}
