package es.guz.auditor.lib.model;

import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-11-28T10:13:11.713+0100")
@StaticMetamodel(Persona.class)
public class Persona_ {
	public static volatile SingularAttribute<Persona, BigInteger> id;
	public static volatile SingularAttribute<Persona, String> apellido1;
	public static volatile SingularAttribute<Persona, String> apellido2;
	public static volatile SingularAttribute<Persona, String> nombre;
	public static volatile ListAttribute<Persona, PersonaIdentificador> identificadores;
	public static volatile SingularAttribute<Persona, String> fnac;
	public static volatile SingularAttribute<Persona, String> sexo;
}
