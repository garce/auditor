package es.guz.auditor.lib.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the personas database table.
 * 
 */
@Entity
@Table(name="personas")
public class Persona implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PERSONAS_ID_GENERATOR", sequenceName="MAIN_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PERSONAS_ID_GENERATOR")
	private BigInteger id;

	private String apellido1;

	private String apellido2;

	private String nombre;

	private String fnac;

	private String sexo;

	//bi-directional many-to-one association to PersonaIdentificador
	@OneToMany(mappedBy="persona", cascade=CascadeType.ALL)
	private List<PersonaIdentificador> identificadores;

	public Persona() {
	}

	public BigInteger getId() {
		return this.id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getApellido1() {
		return this.apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return this.apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getFnac() {
		return fnac;
	}

	public void setFnac(String fnac) {
		this.fnac = fnac;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public List<PersonaIdentificador> getIdentificadores() {
		return this.identificadores;
	}

	public void setIdentificadores(List<PersonaIdentificador> identificadores) {
		this.identificadores = identificadores;
	}
	
	public String getNombreCompleto() {
		return nombre + " " + apellido1 + " " + apellido2;
	}
	
	public String getTodosIdentificadores() {
		if (identificadores != null) {
			StringBuffer buffer = new StringBuffer();
			for (PersonaIdentificador i : identificadores) {
				if (buffer.length() > 0) buffer.append(" | ");
				buffer.append(i);
			}
			return buffer.toString();
		}
		else return null;
	}

	@Override
	public String toString() {
		return "Persona [id=" + id + ", apellido1=" + apellido1 + ", apellido2=" + apellido2 + ", nombre=" + nombre
				+ ", fnac=" + fnac + ", sexo=" + sexo + ", identificadores=" + identificadores + "]";
	}

}