package es.guz.auditor.lib;

public class Constants {
	
	public static final String PERSISTENCE_UNIT = "auditorLib";

	public static final String OBJETIVO_APLICACION = "APLICACION";
	public static final String OBJETIVO_MODULO = "MODULO";
	public static final String OBJETIVO_ACCION = "ACCION";
	public static final String OBJETIVO_PERSONA = "PERSONA";
	public static final String OBJETIVO_PERSONA_IDENTIFICADOR = "PERSONA_IDENTIFICADOR";
	
	public static final String ACCION_APLICACION_INSERT = "APLICACION_INSERT";
	public static final String ACCION_APLICACION_UPDATE = "APLICACION_UPDATE";
	public static final String ACCION_APLICACION_DELETE = "APLICACION_DELETE";
	
	public static final String ACCION_MODULO_INSERT = "MODULO_INSERT";
	public static final String ACCION_MODULO_UPDATE = "MODULO_UPDATE";
	public static final String ACCION_MODULO_DELETE = "MODULO_DELETE";
	
	public static final String ACCION_ACCION_INSERT = "ACCION_INSERT";
	public static final String ACCION_ACCION_UPDATE = "ACCION_UPDATE";
	public static final String ACCION_ACCION_DELETE = "ACCION_DELETE";
	
	public static final String ACCION_PERSONA_INSERT = "PERSONA_INSERT";
	public static final String ACCION_PERSONA_UPDATE = "PERSONA_UPDATE";
	public static final String ACCION_PERSONA_DELETE = "PERSONA_DELETE";
	
	
}
