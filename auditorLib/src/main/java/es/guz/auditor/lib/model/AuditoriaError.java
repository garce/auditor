package es.guz.auditor.lib.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the auditorias_errores database table.
 * 
 */
@Entity
@Table(name="auditorias_errores")
public class AuditoriaError implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="AUDITORIAS_ERRORES_ID_GENERATOR", sequenceName="MAIN_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="AUDITORIAS_ERRORES_ID_GENERATOR")
	private String id;

	private String error;

	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha;

	@Lob
	private byte[] mensaje;

	//uni-directional many-to-one association to Aplicacion
	@ManyToOne
	@JoinColumn(name="aplicacion")
	private Aplicacion aplicacion;

	public AuditoriaError() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getError() {
		return this.error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public byte[] getMensaje() {
		return this.mensaje;
	}

	public void setMensaje(byte[] mensaje) {
		this.mensaje = mensaje;
	}

	public Aplicacion getAplicacion() {
		return this.aplicacion;
	}

	public void setAplicacion(Aplicacion aplicacion) {
		this.aplicacion = aplicacion;
	}

}