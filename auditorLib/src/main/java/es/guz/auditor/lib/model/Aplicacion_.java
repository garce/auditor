package es.guz.auditor.lib.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-11-20T09:50:21.886+0100")
@StaticMetamodel(Aplicacion.class)
public class Aplicacion_ {
	public static volatile SingularAttribute<Aplicacion, Integer> id;
	public static volatile SingularAttribute<Aplicacion, String> codigo;
	public static volatile SingularAttribute<Aplicacion, String> nombre;
	public static volatile ListAttribute<Aplicacion, Accion> acciones;
	public static volatile ListAttribute<Aplicacion, Modulo> modulos;
}
