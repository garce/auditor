package es.guz.auditor.lib.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-11-20T09:50:55.120+0100")
@StaticMetamodel(Modulo.class)
public class Modulo_ {
	public static volatile SingularAttribute<Modulo, Integer> id;
	public static volatile SingularAttribute<Modulo, String> codigo;
	public static volatile SingularAttribute<Modulo, String> descripcion;
	public static volatile SingularAttribute<Modulo, Aplicacion> aplicacion;
	public static volatile SingularAttribute<Modulo, Modulo> padre;
	public static volatile ListAttribute<Modulo, Modulo> hijos;
}
