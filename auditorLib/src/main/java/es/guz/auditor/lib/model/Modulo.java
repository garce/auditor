package es.guz.auditor.lib.model;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the modulos database table.
 * 
 */
@Entity
@Table(name="modulos")
@NamedQueries(
		@NamedQuery(name="Modulo.findByAplicacionCodigo", query="SELECT a FROM Modulo a WHERE a.aplicacion.codigo = :codigoAplicacion AND a.codigo = :codigoModulo"))
public class Modulo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="MODULOS_ID_GENERATOR", sequenceName="MAIN_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="MODULOS_ID_GENERATOR")
	private int id;

	private String codigo;

	private String descripcion;

	//bi-directional many-to-one association to Aplicacion
	@ManyToOne
	@JoinColumn(name="aplicacion")
	private Aplicacion aplicacion;

	//bi-directional many-to-one association to Modulo
	@ManyToOne
	@JoinColumn(name="padre")
	private Modulo padre;

	//bi-directional many-to-one association to Modulo
	@OneToMany(mappedBy="padre")
	private List<Modulo> hijos;

	public Modulo() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Aplicacion getAplicacion() {
		return this.aplicacion;
	}

	public void setAplicacion(Aplicacion aplicacion) {
		this.aplicacion = aplicacion;
	}

	public Modulo getPadre() {
		return this.padre;
	}

	public void setPadre(Modulo padre) {
		this.padre = padre;
	}

	public List<Modulo> getHijos() {
		return this.hijos;
	}

	public void setHijos(List<Modulo> hijos) {
		this.hijos = hijos;
	}
	
	public String getCodigoPadre() {
		if (padre != null) return padre.getCodigo();
		else return null;
	}

}