package es.guz.auditor.lib.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-11-28T09:45:00.588+0100")
@StaticMetamodel(Auditoria.class)
public class Auditoria_ {
	public static volatile SingularAttribute<Auditoria, String> id;
	public static volatile SingularAttribute<Auditoria, String> checksum;
	public static volatile SingularAttribute<Auditoria, String> ip;
	public static volatile SingularAttribute<Auditoria, Date> fecha;
	public static volatile SingularAttribute<Auditoria, Date> fechaInsercion;
	public static volatile SingularAttribute<Auditoria, byte[]> mensaje;
	public static volatile SingularAttribute<Auditoria, byte[]> variable;
	public static volatile SingularAttribute<Auditoria, Accion> accion;
	public static volatile SingularAttribute<Auditoria, Aplicacion> aplicacion;
	public static volatile SingularAttribute<Auditoria, Modulo> modulo;
	public static volatile SingularAttribute<Auditoria, Persona> usuario;
	public static volatile SingularAttribute<Auditoria, Persona> objetivo;
}
