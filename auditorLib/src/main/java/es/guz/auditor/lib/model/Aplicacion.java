package es.guz.auditor.lib.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the aplicaciones database table.
 * 
 */
@Entity
@Table(name="aplicaciones")
@NamedQueries(
		@NamedQuery(name="Aplicacion.findByCodigo", query="SELECT a FROM Aplicacion a WHERE a.codigo = :codigo"))
public class Aplicacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="APLICACIONES_ID_GENERATOR", sequenceName="MAIN_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="APLICACIONES_ID_GENERATOR")
	private int id;

	private String codigo;

	private String nombre;

	//bi-directional many-to-one association to Accion
	@OneToMany(mappedBy="aplicacion")
	private List<Accion> acciones;

	//bi-directional many-to-one association to Modulo
	@OneToMany(mappedBy="aplicacion")
	private List<Modulo> modulos;

	public Aplicacion() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Accion> getAcciones() {
		return this.acciones;
	}

	public void setAcciones(List<Accion> acciones) {
		this.acciones = acciones;
	}

	public List<Modulo> getModulos() {
		return this.modulos;
	}

	public void setModulos(List<Modulo> modulos) {
		this.modulos = modulos;
	}

}