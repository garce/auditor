package es.guz.auditor.lib.model;

import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-11-28T10:12:36.602+0100")
@StaticMetamodel(AuditoriaInterna.class)
public class AuditoriaInterna_ {
	public static volatile SingularAttribute<AuditoriaInterna, String> id;
	public static volatile SingularAttribute<AuditoriaInterna, String> accion;
	public static volatile SingularAttribute<AuditoriaInterna, BigInteger> idObjetivo;
	public static volatile SingularAttribute<AuditoriaInterna, byte[]> mensaje;
	public static volatile SingularAttribute<AuditoriaInterna, String> objetivo;
	public static volatile SingularAttribute<AuditoriaInterna, String> valorAnterior;
}
