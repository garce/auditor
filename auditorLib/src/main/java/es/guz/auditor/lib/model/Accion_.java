package es.guz.auditor.lib.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-11-20T09:50:41.544+0100")
@StaticMetamodel(Accion.class)
public class Accion_ {
	public static volatile SingularAttribute<Accion, Integer> id;
	public static volatile SingularAttribute<Accion, String> codigo;
	public static volatile SingularAttribute<Accion, String> descripcion;
	public static volatile SingularAttribute<Accion, Aplicacion> aplicacion;
}
