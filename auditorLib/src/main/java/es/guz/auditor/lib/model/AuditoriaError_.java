package es.guz.auditor.lib.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-11-19T21:34:34.685+0100")
@StaticMetamodel(AuditoriaError.class)
public class AuditoriaError_ {
	public static volatile SingularAttribute<AuditoriaError, String> id;
	public static volatile SingularAttribute<AuditoriaError, String> error;
	public static volatile SingularAttribute<AuditoriaError, Date> fecha;
	public static volatile SingularAttribute<AuditoriaError, byte[]> mensaje;
	public static volatile SingularAttribute<AuditoriaError, Aplicacion> aplicacion;
}
