package es.guz.auditor.lib.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the usuarios_aplicacion database table.
 * 
 */
@Entity
@Table(name="usuarios_aplicacion")
public class UsuarioAplicacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String user;

	private String password;

	public UsuarioAplicacion() {
	}

	public String getUser() {
		return this.user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}