package es.guz.auditor.lib.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the personas_identificadores database table.
 * 
 */
@Entity
@Table(name="personas_identificadores")
@NamedQueries(
		@NamedQuery(name="PersonaIdentificador.findByTipoIdentificador", query="SELECT p FROM PersonaIdentificador p WHERE p.tipo = :tipo AND p.identificador = :identificador"))
public class PersonaIdentificador implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PERSONAS_IDENTIFICADORES_ID_GENERATOR", sequenceName="MAIN_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PERSONAS_IDENTIFICADORES_ID_GENERATOR")
	private String id;

	private String identificador;

	private String tipo;

	//bi-directional many-to-one association to Persona
	@ManyToOne
	@JoinColumn(name="persona")
	private Persona persona;

	public PersonaIdentificador() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdentificador() {
		return this.identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Persona getPersona() {
		return this.persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	@Override
	public String toString() {
		return "[tipo=" + tipo + ", identificador=" + identificador + "]";
	}
	
	

}