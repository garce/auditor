package es.guz.auditor.lib.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigInteger;


/**
 * The persistent class for the auditoria_interna database table.
 * 
 */
@Entity
@Table(name="auditoria_interna")
public class AuditoriaInterna implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="AUDITORIA_INTERNA_ID_GENERATOR", sequenceName="MAIN_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="AUDITORIA_INTERNA_ID_GENERATOR")
	private String id;

	private String accion;

	@Column(name="id_objetivo")
	private BigInteger idObjetivo;

	@Lob
	private byte[] mensaje;

	private String objetivo;

	@Column(name="valor_anterior")
	private String valorAnterior;

	public AuditoriaInterna() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccion() {
		return this.accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public BigInteger getIdObjetivo() {
		return this.idObjetivo;
	}

	public void setIdObjetivo(BigInteger idObjetivo) {
		this.idObjetivo = idObjetivo;
	}

	public byte[] getMensaje() {
		return this.mensaje;
	}

	public void setMensaje(byte[] mensaje) {
		this.mensaje = mensaje;
	}

	public String getObjetivo() {
		return this.objetivo;
	}

	public void setObjetivo(String objetivo) {
		this.objetivo = objetivo;
	}

	public String getValorAnterior() {
		return this.valorAnterior;
	}

	public void setValorAnterior(String valorAnterior) {
		this.valorAnterior = valorAnterior;
	}

}