package es.guz.auditor.lib.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the acciones database table.
 * 
 */
@Entity
@Table(name="acciones")
@NamedQueries(
		@NamedQuery(name="Accion.findByAplicacionCodigo", query="SELECT a FROM Accion a WHERE a.aplicacion.codigo = :codigoAplicacion AND a.codigo = :codigoModulo"))
public class Accion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ACCIONES_ID_GENERATOR", sequenceName="MAIN_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACCIONES_ID_GENERATOR")
	private int id;

	private String codigo;

	private String descripcion;

	//bi-directional many-to-one association to Aplicacion
	@ManyToOne
	@JoinColumn(name="aplicacion")
	private Aplicacion aplicacion;

	public Accion() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Aplicacion getAplicacion() {
		return this.aplicacion;
	}

	public void setAplicacion(Aplicacion aplicacion) {
		this.aplicacion = aplicacion;
	}

}