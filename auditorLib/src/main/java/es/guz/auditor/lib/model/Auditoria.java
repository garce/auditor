package es.guz.auditor.lib.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the auditorias database table.
 * 
 */
@Entity
@Table(name="auditorias")
public class Auditoria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="AUDITORIAS_ID_GENERATOR", sequenceName="MAIN_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="AUDITORIAS_ID_GENERATOR")
	private String id;

	private String checksum;

	private String ip;

	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="fecha_insercion")
	private Date fechaInsercion;

	@Lob
	private byte[] mensaje;

	@Lob
	private byte[] variable;

	//uni-directional many-to-one association to Accion
	@ManyToOne
	@JoinColumn(name="accion")
	private Accion accion;

	//uni-directional many-to-one association to Aplicacion
	@ManyToOne
	@JoinColumn(name="aplicacion")
	private Aplicacion aplicacion;

	//uni-directional many-to-one association to Modulo
	@ManyToOne
	@JoinColumn(name="modulo")
	private Modulo modulo;

	//uni-directional many-to-one association to Persona
	@ManyToOne
	@JoinColumn(name="usuario")
	private Persona usuario;

	//uni-directional many-to-one association to Persona
	@ManyToOne
	@JoinColumn(name="objetivo")
	private Persona objetivo;

	public Auditoria() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getChecksum() {
		return this.checksum;
	}

	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Date getFechaInsercion() {
		return this.fechaInsercion;
	}

	public void setFechaInsercion(Date fechaInsercion) {
		this.fechaInsercion = fechaInsercion;
	}

	public byte[] getMensaje() {
		return this.mensaje;
	}

	public void setMensaje(byte[] mensaje) {
		this.mensaje = mensaje;
	}

	public byte[] getVariable() {
		return this.variable;
	}

	public void setVariable(byte[] variable) {
		this.variable = variable;
	}

	public Accion getAccion() {
		return this.accion;
	}

	public void setAccion(Accion accion) {
		this.accion = accion;
	}

	public Aplicacion getAplicacion() {
		return this.aplicacion;
	}

	public void setAplicacion(Aplicacion aplicacion) {
		this.aplicacion = aplicacion;
	}

	public Modulo getModulo() {
		return this.modulo;
	}

	public void setModulo(Modulo modulo) {
		this.modulo = modulo;
	}

	public Persona getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Persona usuario) {
		this.usuario = usuario;
	}

	public Persona getObjetivo() {
		return this.objetivo;
	}

	public void setObjetivo(Persona objetivo) {
		this.objetivo = objetivo;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

}