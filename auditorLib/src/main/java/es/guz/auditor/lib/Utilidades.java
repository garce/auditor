package es.guz.auditor.lib;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * OJO
 * 
 * El fichero de configuración está en el monitor, para tener visibilidad y configuración dependiente del entorno, es decir,
 * que si ejecutas -en un futurible- una versión móvil, tenga sus propias constantes aunque la funcionalidad sea la misma.
 * 
 * @author guzman
 *
 */
public abstract class Utilidades {

	public static String getMD5(String cadena) throws NoSuchAlgorithmException {
		MessageDigest m = MessageDigest.getInstance("MD5");
		byte[] data = cadena.getBytes();
		m.update(data, 0, data.length);
		BigInteger i = new BigInteger(1, m.digest());
		return String.format("%1$032X", i);
	}
	
	/**
	 * Acortar una cadena a un tamaño determinado
	 * @param cadena
	 * @param max
	 * @return
	 */
	public static String acortaCadena(String cadena, int max) {
		if (cadena != null && cadena.length() > max && max >= 10) {
			String tmp = cadena.substring(0, max - 4 - 2); // 4 por la finalización del archivo (ej .pdf) y 3 por los ".."
			tmp += "...";
			tmp += cadena.substring(cadena.length() - 4, cadena.length());
			return tmp;
		}
		return cadena;
	}
	
}
