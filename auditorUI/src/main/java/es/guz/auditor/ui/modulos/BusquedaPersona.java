package es.guz.auditor.ui.modulos;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerFactory;
import com.vaadin.addon.jpacontainer.JPAContainerItem;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.InlineDateField;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import es.guz.auditor.lib.Constants;
import es.guz.auditor.lib.model.Auditoria;
import es.guz.auditor.lib.model.Persona;

public class BusquedaPersona extends CustomComponent implements Actualizable {

	private VerticalLayout mainLayout;

	// Contenedores
	JPAContainer<Persona> personas = JPAContainerFactory.make(Persona.class, Constants.PERSISTENCE_UNIT);
	JPAContainer<Auditoria> auditorias = JPAContainerFactory.make(Auditoria.class, Constants.PERSISTENCE_UNIT);

	// Componentes
	TextField persona = new TextField();
	Button buscar = new Button("Buscar por identificador");
	Button limpiar = new Button("Limpiar resultados");
	Table table = new Table("Auditorias", auditorias);
	Table tablePersonas = new Table("Coincidencias", personas);
	InlineDateField fecha = new InlineDateField("Fecha");

	public BusquedaPersona(String caption) {

		persona.setCaption(caption);
		
		mainLayout = new VerticalLayout();
		mainLayout.setSizeFull();
		setCompositionRoot(mainLayout);
		
		persona.setImmediate(true);
		tablePersonas.setImmediate(true);
		fecha.setImmediate(true);
		table.setImmediate(true);
		
		fecha.setValue(new Date());
		fecha.setResolution(Resolution.DAY);
		
		// Tamaños de las tablas
		tablePersonas.setSizeFull();
		tablePersonas.setHeight("220px");
		table.setSizeFull();

		// Selección de búsqueda
		HorizontalLayout layoutH = new HorizontalLayout();
		layoutH.setSpacing(true);
		layoutH.setMargin(true);
		layoutH.setSizeFull();
		
		FormLayout layout = new FormLayout();
		layout.setSpacing(true);
		layout.setMargin(false);
		layout.setSizeFull();
		layout.addComponents(persona, buscar, limpiar);
		layoutH.addComponents(layout, tablePersonas, fecha);
		layoutH.setExpandRatio(layout, 1);
		layoutH.setExpandRatio(tablePersonas, 3);
		
		// Configurar tabla personas
		tablePersonas.setSelectable(true);
		tablePersonas.setMultiSelect(false);
		tablePersonas.setVisibleColumns(new String[] {"nombreCompleto", "todosIdentificadores", "fnac", "sexo"});
		tablePersonas.setColumnHeader("todosIdentificadores", "Identificadores");
		tablePersonas.setColumnHeader("nombreCompleto", "Nombre");
		tablePersonas.setColumnHeader("fnac", "Fecha Nacimiento");
		
		// Configurar tabla auditorias
		table.setPageLength(20);
		table.setCacheRate(0.1);
		table.setSelectable(true);
		table.setMultiSelect(false);
		auditorias.addNestedContainerProperty("aplicacion.nombre");
		auditorias.addNestedContainerProperty("modulo.descripcion");
		auditorias.addNestedContainerProperty("accion.descripcion");
		auditorias.addNestedContainerProperty("usuario.nombreCompleto");
		auditorias.addNestedContainerProperty("objetivo.nombreCompleto");
		table.setVisibleColumns(new String[] { "fechaInsercion", "fecha", "aplicacion.nombre", "modulo.descripcion",
				"accion.descripcion", "ip", "usuario.nombreCompleto", "objetivo.nombreCompleto" });
		table.setColumnHeader("fechaInsercion", "Fecha Inserción");
		table.setColumnHeader("fecha", "Fecha");
		table.setColumnHeader("aplicacion.nombre", "Aplicación");
		table.setColumnHeader("modulo.descripcion", "Módulo");
		table.setColumnHeader("accion.descripcion", "Acción");
		table.setColumnHeader("usuario.nombreCompleto", "Usuario");
		table.setColumnHeader("objetivo.nombreCompleto", "Objetivo");
		
		// Configurar contenedor de las auditorias
		auditorias.sort(new String[]{"fechaInsercion"}, new boolean[]{false});

		// Añadir los componentes
		mainLayout.addComponents(layoutH, table);
		
		// Acciones de los botones
		buscar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				personas.removeAllContainerFilters();
				personas.addContainerFilter(new Compare.Equal("identificadores.identificador", persona.getValue()));
			}
		});
		limpiar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				personas.removeAllContainerFilters();
			}
		});
		// Acciones de la tabla
		tablePersonas.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				actualizaTabla();
			}
		});

		// Configurar la acción de la tabla
		table.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (table.getValue() != null) {
					JPAContainerItem<Auditoria> item = (JPAContainerItem<Auditoria>) table.getItem(table.getValue());
					getUI().addWindow(new AuditoriaWindow(item.getEntity()));
				}
				
			}
		});
		
		fecha.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				actualizaTabla();
			}
		});
		
		actualizaTabla();

	}
	private void actualizaTabla() {

		// Quitar los filtros
		auditorias.removeAllContainerFilters();

		if ("Usuario".equals(persona.getCaption())) {
			auditorias.addContainerFilter(new Compare.Equal("usuario.id", tablePersonas.getValue()));
		} else if ("Objetivo".equals(persona.getCaption())) {
			auditorias.addContainerFilter(new Compare.Equal("objetivo.id", tablePersonas.getValue()));
		}

		GregorianCalendar gIni = new GregorianCalendar();
		gIni.setTime(fecha.getValue());
		gIni.set(Calendar.HOUR, gIni.getActualMinimum(Calendar.HOUR));
		gIni.set(Calendar.MINUTE, gIni.getActualMinimum(Calendar.MINUTE));
		gIni.set(Calendar.SECOND, gIni.getActualMinimum(Calendar.SECOND));
		gIni.set(Calendar.MILLISECOND, gIni.getActualMinimum(Calendar.MILLISECOND));
		GregorianCalendar gFin = new GregorianCalendar();
		gFin.setTime(fecha.getValue());
		gFin.set(Calendar.HOUR, 23);
		gFin.set(Calendar.MINUTE, 59);
		gFin.set(Calendar.SECOND, 59);
		gFin.set(Calendar.MILLISECOND, gFin.getActualMaximum(Calendar.MILLISECOND));

		auditorias.addContainerFilter(new Compare.GreaterOrEqual("fechaInsercion", gIni.getTime()));
		auditorias.addContainerFilter(new Compare.LessOrEqual("fechaInsercion", gFin.getTime()));
		
	}

	@Override
	public void refresh() {
		auditorias.refresh();
	}

}
