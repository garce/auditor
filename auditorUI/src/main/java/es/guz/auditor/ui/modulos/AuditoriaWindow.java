package es.guz.auditor.ui.modulos;

import java.text.SimpleDateFormat;

import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Window;

import es.guz.auditor.lib.model.Auditoria;

public class AuditoriaWindow extends Window {

	public AuditoriaWindow(Auditoria auditoria) {

		center();
		setSizeFull();
		setModal(true);
		setResizable(false);
		
		FormLayout layout = new FormLayout();
		layout.setMargin(true);
		setContent(layout);
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		
		Label fechaInsercion = new Label(sdf.format(auditoria.getFechaInsercion()));
		fechaInsercion.setCaption("Fecha Inserción");

		Label fecha = new Label("-");
		fecha.setCaption("Fecha");
		if (auditoria.getFecha() != null) {
			fecha.setValue(sdf.format(auditoria.getFecha()));
		}

		Label aplicacion = new Label("-");
		aplicacion.setCaption("Aplicación");
		if (auditoria.getAplicacion() != null) {
			aplicacion.setValue(auditoria.getAplicacion().getNombre());
		}

		Label modulo = new Label("-");
		modulo.setCaption("Módulo");
		if (auditoria.getModulo() != null) {
			modulo.setValue(auditoria.getModulo().getDescripcion());
		}

		Label accion = new Label("-");
		accion.setCaption("Acción");
		if (auditoria.getAccion() != null) {
			accion.setValue(auditoria.getAccion().getDescripcion());
		}

		Label usuario = new Label("-");
		usuario.setCaption("Usuario");
		if (auditoria.getUsuario() != null) {
			usuario.setValue(auditoria.getUsuario().getNombreCompleto());
		}

		Label objetivo = new Label("-");
		objetivo.setCaption("Objetivo");
		if (auditoria.getObjetivo() != null) {
			objetivo.setValue(auditoria.getObjetivo().getNombreCompleto());
		}

		Label variable = new Label("-");
		variable.setCaption("Variable");
		if (auditoria.getVariable() != null) {
			variable.setValue(new String(auditoria.getVariable()));
		}

		Label mensaje = new Label("-");
		mensaje.setCaption("Mensaje");
		if (auditoria.getMensaje() != null) {
			mensaje.setValue(new String(auditoria.getMensaje()));
		}

		Label checksum = new Label("-");
		checksum.setCaption("Checksum");
		if (auditoria.getChecksum() != null) {
			checksum.setValue(auditoria.getChecksum().toString());
		}
		
		layout.addComponents(fechaInsercion, fecha, aplicacion, modulo, accion, usuario, objetivo, variable, mensaje, checksum);
		
	}
	
}
