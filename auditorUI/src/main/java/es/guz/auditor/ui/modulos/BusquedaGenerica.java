package es.guz.auditor.ui.modulos;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerFactory;
import com.vaadin.addon.jpacontainer.JPAContainerItem;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.filter.And;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.InlineDateField;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

import es.guz.auditor.lib.Constants;
import es.guz.auditor.lib.model.Accion;
import es.guz.auditor.lib.model.Aplicacion;
import es.guz.auditor.lib.model.Auditoria;
import es.guz.auditor.lib.model.Modulo;

public class BusquedaGenerica extends CustomComponent implements Actualizable {

	private VerticalLayout mainLayout;

	// Contenedores
	JPAContainer<Aplicacion> aplicaciones = JPAContainerFactory.make(Aplicacion.class, Constants.PERSISTENCE_UNIT);
	JPAContainer<Modulo> modulos = JPAContainerFactory.make(Modulo.class, Constants.PERSISTENCE_UNIT);
	JPAContainer<Accion> acciones = JPAContainerFactory.make(Accion.class, Constants.PERSISTENCE_UNIT);
	JPAContainer<Auditoria> auditorias = JPAContainerFactory.make(Auditoria.class, Constants.PERSISTENCE_UNIT);

	// Componentes
	private ListSelect listAplicaciones = new ListSelect("Aplicación");
	private ListSelect listModulos = new ListSelect("Módulos");
	private ListSelect listAcciones = new ListSelect("Acciones");
	private InlineDateField fecha = new InlineDateField("Fecha");
	private Table table = new Table("Auditorias", auditorias);

	public BusquedaGenerica() {

		mainLayout = new VerticalLayout();
		mainLayout.setSizeFull();
		setCompositionRoot(mainLayout);

		listAplicaciones.setImmediate(true);
		listAplicaciones.setImmediate(true);
		listAcciones.setImmediate(true);
		fecha.setImmediate(true);
		table.setImmediate(true);
		
		table.setSizeFull();
		table.setPageLength(20);
		table.setCacheRate(0.1);
		table.setSelectable(true);
		table.setMultiSelect(false);
		
		fecha.setValue(new Date());
		fecha.setResolution(Resolution.DAY);

		// Selección de búsqueda
		HorizontalLayout layout = new HorizontalLayout();
		layout.setSpacing(true);
		layout.setMargin(true);
		layout.setSizeFull();
		layout.addComponents(listAplicaciones, listModulos, listAcciones, fecha);

		// Asignar contenedores
		listAplicaciones.setContainerDataSource(aplicaciones);
		listModulos.setContainerDataSource(modulos);
		listAcciones.setContainerDataSource(acciones);

		// Configurar componentes de lista
		listAplicaciones.setMultiSelect(false);
		listModulos.setMultiSelect(false);
		listAcciones.setMultiSelect(false);
		listAplicaciones.setNullSelectionAllowed(true);
		listModulos.setNullSelectionAllowed(true);
		listAcciones.setNullSelectionAllowed(true);
		listAplicaciones.setItemCaptionPropertyId("nombre");
		listModulos.setItemCaptionPropertyId("descripcion");
		listAcciones.setItemCaptionPropertyId("descripcion");
		listModulos.setEnabled(false);
		listAcciones.setEnabled(false);

		// Configurar tabla
		auditorias.addNestedContainerProperty("aplicacion.nombre");
		auditorias.addNestedContainerProperty("modulo.descripcion");
		auditorias.addNestedContainerProperty("accion.descripcion");
		auditorias.addNestedContainerProperty("usuario.nombreCompleto");
		auditorias.addNestedContainerProperty("objetivo.nombreCompleto");
		table.setVisibleColumns(new String[] { "fechaInsercion", "fecha", "aplicacion.nombre", "modulo.descripcion",
				"accion.descripcion", "ip", "usuario.nombreCompleto", "objetivo.nombreCompleto" });
		table.setColumnHeader("fechaInsercion", "Fecha Inserción");
		table.setColumnHeader("fecha", "Fecha");
		table.setColumnHeader("aplicacion.nombre", "Aplicación");
		table.setColumnHeader("modulo.descripcion", "Módulo");
		table.setColumnHeader("accion.descripcion", "Acción");
		table.setColumnHeader("usuario.nombreCompleto", "Usuario");
		table.setColumnHeader("objetivo.nombreCompleto", "Objetivo");
		
		// Configurar contenedor de las auditorias
		auditorias.sort(new String[]{"fechaInsercion"}, new boolean[]{false});

		// Añadir los componentes
		mainLayout.addComponents(layout, table);
		
		// Configurar acciones de las listas
		listAplicaciones.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (event.getProperty().getValue() != null) {
					// Actualizar estado
					listModulos.setEnabled(true);
					listAcciones.setEnabled(true);
					// Actualizar filtros
					modulos.removeAllContainerFilters();
					modulos.addContainerFilter(new Compare.Equal("aplicacion.id", listAplicaciones.getValue()));
					acciones.removeAllContainerFilters();
					acciones.addContainerFilter(new Compare.Equal("aplicacion.id", listAplicaciones.getValue()));
				} else {
					// Actualizar estado
					listModulos.setEnabled(false);
					listAcciones.setEnabled(false);
					// Actualizar filtros
					modulos.removeAllContainerFilters();
					acciones.removeAllContainerFilters();
				}
				actualizaTabla();
			}
		});
		listModulos.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				actualizaTabla();
			}
		});
		listAcciones.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				actualizaTabla();
			}
		});
		fecha.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				actualizaTabla();
			}
		});
		
		// Configurar la acción de la tabla
		table.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (table.getValue() != null) {
					JPAContainerItem<Auditoria> item = (JPAContainerItem<Auditoria>) table.getItem(table.getValue());
					getUI().addWindow(new AuditoriaWindow(item.getEntity()));
				}
				
			}
		});
		
		actualizaTabla();

	}
	
	private void actualizaTabla() {

		// Quitar los filtros
		auditorias.removeAllContainerFilters();
		
		// Comprobar si hay aplicación
		if (listAplicaciones.getValue() != null) {
			auditorias.addContainerFilter(new Compare.Equal("aplicacion.id", listAplicaciones.getValue()));
		}
		// Comprobar si hay módulo
		if (listModulos.getValue() != null) {
			auditorias.addContainerFilter(new Compare.Equal("modulo.id", listModulos.getValue()));
		}
		// Comprobar si hay acción
		if (listAcciones.getValue() != null) {
			auditorias.addContainerFilter(new Compare.Equal("accion.id", listAcciones.getValue()));
		}

		GregorianCalendar gIni = new GregorianCalendar();
		gIni.setTime(fecha.getValue());
		gIni.set(Calendar.HOUR, gIni.getActualMinimum(Calendar.HOUR));
		gIni.set(Calendar.MINUTE, gIni.getActualMinimum(Calendar.MINUTE));
		gIni.set(Calendar.SECOND, gIni.getActualMinimum(Calendar.SECOND));
		gIni.set(Calendar.MILLISECOND, gIni.getActualMinimum(Calendar.MILLISECOND));
		GregorianCalendar gFin = new GregorianCalendar();
		gFin.setTime(fecha.getValue());
		gFin.set(Calendar.HOUR, 23);
		gFin.set(Calendar.MINUTE, 59);
		gFin.set(Calendar.SECOND, 59);
		gFin.set(Calendar.MILLISECOND, gFin.getActualMaximum(Calendar.MILLISECOND));

		auditorias.addContainerFilter(new Compare.GreaterOrEqual("fechaInsercion", gIni.getTime()));
		auditorias.addContainerFilter(new Compare.LessOrEqual("fechaInsercion", gFin.getTime()));
		
	}

	@Override
	public void refresh() {
		aplicaciones.refresh();
		modulos.refresh();
		acciones.refresh();
		auditorias.refresh();
	}

}
