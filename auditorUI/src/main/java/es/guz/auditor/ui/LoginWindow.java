package es.guz.auditor.ui;

import java.security.NoSuchAlgorithmException;

import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerFactory;
import com.vaadin.data.Container.Filter;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import es.guz.auditor.lib.Constants;
import es.guz.auditor.lib.Utilidades;
import es.guz.auditor.lib.model.UsuarioAplicacion;

public class LoginWindow extends Window {
	
	// Contenedor de los usuarios
	JPAContainer<UsuarioAplicacion> usuarios =
		    JPAContainerFactory.make(UsuarioAplicacion.class, Constants.PERSISTENCE_UNIT);

	private TextField userField = new TextField();
	private PasswordField passField = new PasswordField();
	private Button login = new Button("Entrar");

	public LoginWindow() {
		
		this.setCaption("Introduce usuario y contraseña");
		this.setResizable(false);
		this.setClosable(false);
//		this.setModal(true);
		this.setWidth("510px");
		this.setHeight("100px");
		this.center();

        userField.setInputPrompt("Usuario");
        passField.setInputPrompt("Password");
        
        userField.setValue("admin");
        passField.setValue("puerta");
        
        VerticalLayout vld = new VerticalLayout();
		setContent(vld);
        vld.setMargin(true);
        vld.setSizeFull();
        
		HorizontalLayout rld = new HorizontalLayout();
		vld.addComponent(rld);
		rld.setSpacing(true);
		rld.addComponent(userField);
		rld.addComponent(passField);
		rld.addComponent(login);
		vld.setComponentAlignment(rld, Alignment.MIDDLE_CENTER);
		
		login.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				
//				Filter filter = new Compare.Equal("user", userField.getValue());
//				usuarios.addContainerFilter(filter);
				EntityItem<UsuarioAplicacion> user =  usuarios.getItem(userField.getValue());
				
				try {
					
					if (user != null && passField.getValue() != null && Utilidades.getMD5(passField.getValue()).equals(user.getEntity().getPassword())) {
						// Login OK
						((AuditorUI) getUI().getCurrent()).doLogin();
					} else {
						// Login KO
						Notification.show("Usuario o contraseña erróneo", Notification.Type.WARNING_MESSAGE);
					}
					
				} catch (NoSuchAlgorithmException e) {
					Notification.show("Error de configuración", Notification.Type.ERROR_MESSAGE);
				}
				
			}
		});
		login.setClickShortcut(KeyCode.ENTER);
		userField.focus();
		
	}

}
