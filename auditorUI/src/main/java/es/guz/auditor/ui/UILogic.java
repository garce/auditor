package es.guz.auditor.ui;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import es.guz.auditor.lib.Constants;
import es.guz.auditor.lib.model.UsuarioAplicacion;

public class UILogic {

	private static UILogic instance;

	private UILogic() {
	}

	public static UILogic getInstance() {
		if (instance == null) {
			instance = new UILogic();
		}
		return instance;
	}

	public String getSample() {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory(Constants.PERSISTENCE_UNIT);
		EntityManager em = emf.createEntityManager();

		UsuarioAplicacion perfil = em.find(UsuarioAplicacion.class, "admin");
		return perfil.getPassword();
		
	}
}
