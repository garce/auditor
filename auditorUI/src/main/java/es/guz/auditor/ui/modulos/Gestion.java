package es.guz.auditor.ui.modulos;

import java.security.NoSuchAlgorithmException;

import javax.persistence.EntityManager;

import org.vaadin.dialogs.ConfirmDialog;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerFactory;
import com.vaadin.addon.jpacontainer.JPAContainerItem;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.FieldEvents.FocusEvent;
import com.vaadin.event.FieldEvents.FocusListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import es.guz.auditor.lib.Constants;
import es.guz.auditor.lib.Utilidades;
import es.guz.auditor.lib.model.Modulo;
import es.guz.auditor.lib.model.UsuarioAplicacion;

public class Gestion extends CustomComponent implements Actualizable {
	
    final Action actionAdd = new Action("Añadir");
    final Action actionEdit = new Action("Editar");
    final Action actionDelete = new Action("Eliminar");
	
	// Contenedor de los usuarios
	JPAContainer<UsuarioAplicacion> usuarios =
		    JPAContainerFactory.make(UsuarioAplicacion.class, Constants.PERSISTENCE_UNIT);
	// Contenedor de los módulos
	JPAContainer<Modulo> modulos =
		    JPAContainerFactory.make(Modulo.class, Constants.PERSISTENCE_UNIT);

	private HorizontalLayout mainLayout;

	public Gestion() {
		mainLayout = new HorizontalLayout();
		setCompositionRoot(mainLayout);

		mainLayout.setSizeFull();
		mainLayout.setSpacing(true);
		mainLayout.setMargin(true);
		
		Table tableUsuarios = new Table("Usuarios", usuarios);
		tableUsuarios.setSizeFull();
		tableUsuarios.setVisibleColumns(new String[]{"user"});
		mainLayout.addComponent(tableUsuarios);
		
		Table tableModulos = new Table("Modulos", modulos);
		tableModulos.setSizeFull();
		tableModulos.setVisibleColumns(new String[]{"id", "codigo", "descripcion", "codigoPadre"});
		tableModulos.setColumnHeader("codigoPadre", "padre");
		mainLayout.addComponent(tableModulos);
		
		// Acciones
		tableUsuarios.addActionHandler(new Handler() {
			
			@Override
			public void handleAction(Action action, Object sender, final Object target) {
				if (actionAdd == action) {
					getUI().addWindow(new UsuarioForm());
				} else if (actionEdit == action) {
					JPAContainerItem<UsuarioAplicacion> o = (JPAContainerItem<UsuarioAplicacion>) usuarios.getItem(target);
					if (o != null) { // Comprobamos que haya seleccionado algo
						UsuarioAplicacion user = o.getEntity();
						getUI().addWindow(new UsuarioForm(user));
					}
				} else if (actionDelete == action) {
					ConfirmDialog.show(getUI(), "Confirmar eliminación", "¿Seguro que desea eliminar el usuario " + target + "?", "Aceptar", "Cancelar",
							new ConfirmDialog.Listener() {

								public void onClose(ConfirmDialog dialog) {
									if (dialog.isConfirmed()) {
										// Confirmed to continue
										usuarios.removeItem(target);
									} else {
										// User did not confirm
										// No hacer nada
									}
								}
							});
				}
			}
			
			@Override
			public Action[] getActions(Object target, Object sender) {
				return new Action[] { actionAdd, actionEdit, actionDelete };
			}
			
		});
		tableModulos.addActionHandler(new Handler() {
			
			@Override
			public void handleAction(Action action, Object sender, Object target) {
				if (actionEdit == action) {
					JPAContainerItem<Modulo> o = (JPAContainerItem<Modulo>) modulos.getItem(target);
					if (o != null) { // Comprobamos que haya seleccionado algo
						Modulo modulo = o.getEntity();
						getUI().addWindow(new ModuloForm(modulo));
					}
				}
			}
			
			@Override
			public Action[] getActions(Object target, Object sender) {
				return new Action[] { actionEdit };
			}
			
		});
		
		
	}
	
	class UsuarioForm extends Window {
		
		private TextField user = new TextField("Usuario");
		private PasswordField password = new PasswordField("Contraseña");
		
		private final Window window = this;
		private VerticalLayout layout;
		
		private Button btnAceptar = new Button("Aceptar");
		
		/**
		 * CREACIÓN
		 */
		public UsuarioForm() {

			init();
			this.setCaption("Añadir usuario");
			
			btnAceptar.addClickListener(new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
					try {
						UsuarioAplicacion usuario = new UsuarioAplicacion();
						usuario.setUser(user.getValue());
						usuario.setPassword(Utilidades.getMD5(password.getValue()));
						usuarios.addEntity(usuario);
					} catch (NoSuchAlgorithmException | UnsupportedOperationException | IllegalStateException e) {
						e.printStackTrace();
						Notification.show("Error al insertar el usuario: " + e.getMessage(), Type.ERROR_MESSAGE);
					} finally {
						getUI().removeWindow(window);
					}
				}
			});
			
		}
		
		/**
		 * EDICIÓN
		 */
		public UsuarioForm(final UsuarioAplicacion usuario) {
			
			init();
			this.setCaption("Editar usuario");
			
			user.setEnabled(false);
			user.setValue(usuario.getUser());
			password.setValue(usuario.getPassword());
			
			password.addFocusListener(new FocusListener() {
				@Override
				public void focus(FocusEvent event) {
					((PasswordField) event.getComponent()).setValue("");
				}
			});
			
			btnAceptar.addClickListener(new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
					try {
						if (!usuario.getPassword().equals(password.getValue())) {
							EntityManager em = JPAContainerFactory.createEntityManagerForPersistenceUnit(Constants.PERSISTENCE_UNIT);
							usuario.setPassword(Utilidades.getMD5(password.getValue()));
							em.getTransaction().begin();
							em.merge(usuario);
							em.getTransaction().commit();
							em.flush();
							usuarios.refresh();
						}
					} catch (NoSuchAlgorithmException | UnsupportedOperationException | IllegalStateException e) {
						e.printStackTrace();
						Notification.show("Error al insertar el usuario: " + e.getMessage(), Type.ERROR_MESSAGE);
					} finally {
						getUI().removeWindow(window);
					}
				}
			});
			
		}
		
		public void init() {
			this.setModal(true);
			this.setResizable(false);
			this.setWidth("320px");
			this.setHeight("280px");
			this.center();
			
			layout = new VerticalLayout();
			layout.setMargin(true);
			layout.setSpacing(true);
			this.setContent(layout);
			
			FormLayout form = new FormLayout();
			form.addComponents(user, password);
			
			layout.addComponents(form, btnAceptar);
			layout.setComponentAlignment(btnAceptar, Alignment.BOTTOM_CENTER);
		}
		
	}
	
	class ModuloForm extends Window {
		
		private NativeSelect padre = new NativeSelect("Padre");
		
		private final Window window = this;
		private VerticalLayout layout;
		
		private Button btnAceptar = new Button("Aceptar");
		
		public ModuloForm(final Modulo modulo) {
			
			this.setModal(true);
			this.setResizable(false);
			this.setWidth("320px");
			this.setHeight("200px");
			this.center();
			
			layout = new VerticalLayout();
			layout.setMargin(true);
			layout.setSpacing(true);
			this.setContent(layout);
			
			padre.setNullSelectionAllowed(false);
			
			FormLayout form = new FormLayout();
			form.addComponents(padre);
			
			layout.addComponents(form, btnAceptar);
			layout.setComponentAlignment(btnAceptar, Alignment.BOTTOM_CENTER);
			
			// Sin padre
			padre.addItem(0);
			padre.setItemCaption(0, "ninguno");

			// Insertar los módulos
			for (Object m : modulos.getItemIds()) {
				padre.addItem(m);
				JPAContainerItem<Modulo> mod = (JPAContainerItem<Modulo>) modulos.getItem(m);
				padre.setItemCaption(m, mod.getEntity().getDescripcion());
			}
			
			// Seleccionar al padre
			if (modulo.getPadre() == null) {
				padre.setValue(0);
			} else {
				padre.setValue(modulo.getPadre().getId());
			}
			
			btnAceptar.addClickListener(new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
					try {
						EntityManager em = JPAContainerFactory.createEntityManagerForPersistenceUnit(Constants.PERSISTENCE_UNIT);
						Modulo pater = em.find(Modulo.class, padre.getValue());
						modulo.setPadre(pater);
						em.getTransaction().begin();
						em.merge(modulo);
						em.getTransaction().commit();
//						em.flush();
						modulos.refresh();
					} catch (UnsupportedOperationException | IllegalStateException e) {
						e.printStackTrace();
						Notification.show("Error al insertar el usuario: " + e.getMessage(), Type.ERROR_MESSAGE);
					} finally {
						getUI().removeWindow(window);
					}
				}
			});
			
		}
		
	}

	@Override
	public void refresh() {
		usuarios.refresh();
		modulos.refresh();
	}

}
