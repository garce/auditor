package es.guz.auditor.ui.modulos;

public interface Actualizable {
	public void refresh();
}
