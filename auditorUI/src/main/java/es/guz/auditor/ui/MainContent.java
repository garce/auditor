package es.guz.auditor.ui;

import org.vaadin.dialogs.ConfirmDialog;

import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Label;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TabSheet.SelectedTabChangeListener;
import com.vaadin.ui.UIDetachedException;
import com.vaadin.ui.themes.ValoTheme;

import es.guz.auditor.ui.modulos.Actualizable;
import es.guz.auditor.ui.modulos.BusquedaGenerica;
import es.guz.auditor.ui.modulos.BusquedaPersona;
import es.guz.auditor.ui.modulos.Gestion;

public class MainContent extends AbsoluteLayout {

	private TabSheet tabs;
	private Button cerrarSesion;
	private Label nombre;
	
	public MainContent() {
		buildMainLayout();

        new RefreshThread().start();
	}
	

	private void buildMainLayout() {
		// common part: create layout
		this.setImmediate(false);
		this.setWidth("100%");
		this.setHeight("100%");
		
		// top-level component properties
		setWidth("100.0%");
		setHeight("100.0%");
		
		// nombre
		nombre = new Label();
		nombre.setImmediate(false);
		nombre.setWidth("-1px");
		nombre.setHeight("-1px");
		nombre.setValue("Guzmán Arce");
		this.addComponent(nombre, "top:10.0px;right:30.0px;");
		
		// cerrarSesion
		cerrarSesion = new Button();
		cerrarSesion.setCaption("cerrar sesión");
		cerrarSesion.setImmediate(true);
		cerrarSesion.setDescription("cerrar sesión");
		cerrarSesion.setWidth("-1px");
		cerrarSesion.setHeight("-1px");
		this.addComponent(cerrarSesion, "top:25.0px;right:18.0px;");
		
		// tabs
		tabs = new TabSheet();
		tabs.setImmediate(false);
		tabs.setWidth("100.0%");
		tabs.setHeight("100.0%");
		this.addComponent(tabs,
				"top:60.0px;right:0.0px;bottom:0.0px;left:0.0px;");
		
		cerrarSesion.setStyleName(ValoTheme.BUTTON_BORDERLESS + " " + ValoTheme.BUTTON_TINY);
		tabs.setStyleName(ValoTheme.TABSHEET_FRAMED + " " + ValoTheme.TABSHEET_COMPACT_TABBAR);

		// Acciones
		cerrarSesion.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				ConfirmDialog.show(getUI(), "Salir de la sesión", "¿Seguro que desea finalizar la sesión?", "Aceptar", "Cancelar",
						new ConfirmDialog.Listener() {

							public void onClose(ConfirmDialog dialog) {
								if (dialog.isConfirmed()) {
									// Confirmed to continue
									
							        // Close the VaadinServiceSession
							        getUI().getSession().close();

							        // Invalidate underlying session instead if login info is stored there
							        // VaadinService.getCurrentRequest().getWrappedSession().invalidate();

							        // Redirect to avoid keeping the removed UI open in the browser
							        getUI().getPage().setLocation("");
							        
								} else {
									// User did not confirm
									// No hacer nada
								}
							}
						});
			}
		});

		tabs.addTab(new BusquedaGenerica(), "Búsqueda genérica");
		tabs.addTab(new BusquedaPersona("Usuario"), "Búsqueda por usuario");
		tabs.addTab(new BusquedaPersona("Objetivo"), "Búsqueda por objetivo");
		tabs.addTab(new Gestion(), "Gestión");
		
		tabs.addSelectedTabChangeListener(new SelectedTabChangeListener() {
			@Override
			public void selectedTabChange(SelectedTabChangeEvent event) {
				if (event.getTabSheet().getSelectedTab() instanceof Actualizable) {
					((Actualizable) event.getTabSheet().getSelectedTab()).refresh();
				}
			}
		});
		
	}
	
	class RefreshThread extends Thread {
        @Override
        public void run() {
        
        	while (true) {
        	
	            // Do initialization which takes some time.
	            // Here represented by a 1s sleep
	            try {
	                Thread.sleep(30000);
	            } catch (InterruptedException e) {
	            }
	
	            // Init done, update the UI after doing locking
	            try {
					getUI().getCurrent().access(new Runnable() {
					    @Override
					    public void run() {
					        // Here the UI is locked and can be updated
					    	if (tabs.getSelectedTab() instanceof Actualizable) {
					    		((Actualizable) tabs.getSelectedTab()).refresh();
					    	}
//					    	getUI().push();
					    }
					});
				} catch (UIDetachedException e) {
					// Si se produce una excepción es que se ha cerrado la sesión, finalizamos el bucle
					break;
				}
            
        	}
        }
	}
	

}
