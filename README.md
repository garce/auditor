# README #

Voy a comentar un poco los pasos para la utilización de este proyecto

### What is this repository for? ###

Existen 4 proyectos:

* auditorLib son los objetos JPA para integración con base de datos y clases comunes.

* auditorREST es la aplicación REST (utilizando Jersey) de entrada de datos en formato JSON.

* auditorClient es una librería Java para integración con el proyecto, sólo habría que utilizar este jar para conectar con la aplicación auditorREST.

* auditorUI es la aplicación de monitorización.


### How do I get set up? ###

Al clonar el repositorio se crean los cuatro proyectos.

Lo primero sería configurar el persistence.xml, con la configuración de base de datos correspondiente, y si se desean crear las tablas a través de JPA o se va a utilizar el fichero provisto (auditor.mwb, en la raiz de los proyectos).

Una vez configurado el persistence, cada proyecto tiene que ejecutar el siguiente código:

**auditorLib**

```
#!bash

mvn clean install
```

**auditorREST**

```
#!bash

mvn clean package
```

**auditorUI**

```
#!bash

mvn clean vaadin:compile package
```


**auditorClient**

```
#!bash

mvn clean install
```

Una vez ejecutados los comandos, en la carpeta target se encontrará el jar o el war correspondiente, además de que se añadirán en el repositorio local los jar correspondientes para poder utilizar las librerías directamente con maven.

### Contribution guidelines ###

Me vendría bien que alguien revisara los tests, ya que ahora mismo sólo se comprueba la capa de acceso a datos.

### Who do I talk to? ###

Contactar directamente conmigo, a través de bitbucket.