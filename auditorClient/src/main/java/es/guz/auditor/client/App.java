package es.guz.auditor.client;

import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import es.guz.auditor.client.beans.Accion;
import es.guz.auditor.client.beans.Aplicacion;
import es.guz.auditor.client.beans.Auditoria;
import es.guz.auditor.client.beans.Identificador;
import es.guz.auditor.client.beans.Modulo;
import es.guz.auditor.client.beans.Persona;

/**
 * Hello world!
 * 
 */
public class App {
	public static void main(String[] args) {
		get();
		post();
	}
	
	public static void get() {
		
		try {

			Client client = Client.create();

			WebResource webResource = client.resource("http://localhost:8080/auditorREST/entrada");

			ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			String output = response.getEntity(String.class);

			System.out.println("Output from Server .... \n");
			System.out.println(output);

			Gson gson = new Gson();
			Auditoria auditoria = gson.fromJson(output, Auditoria.class);

			System.out.println("Objeto parseado .... \n");
			System.out.println(auditoria);

		} catch (Exception e) {

			e.printStackTrace();

		}
		
	}
	
	public static void post() {

		try {

	    	Modulo modulo = new Modulo();
	    	modulo.setCodigo("M1");
	    	modulo.setDescripcion("Módulo 1");
	    	
	    	Accion accion = new Accion();
	    	accion.setCodigo("AC1");
	    	accion.setDescripcion("Acción 1");
	    	
	    	Persona usuario = new Persona();
	    	usuario.setNombre("usuario");
	    	usuario.setApellido1("usuario-ape1");
	    	usuario.setApellido2("usuario-ape2");
	    	Identificador usuarioId1 = new Identificador();
	    	usuarioId1.setTipo("DNI");
	    	usuarioId1.setIdentificador("1234");
	    	usuario.getIdentificadores().add(usuarioId1);
	    	Identificador usuarioId2 = new Identificador();
	    	usuarioId2.setTipo("NIF");
	    	usuarioId2.setIdentificador("6789");
	    	usuario.getIdentificadores().add(usuarioId2);
	    	
	    	Persona objetivo = new Persona();
	    	objetivo.setNombre("objetivo");
	    	objetivo.setApellido1("objetivo-ape1");
	    	objetivo.setApellido2("objetivo-ape2");
	    	Identificador usuarioId3 = new Identificador();
	    	usuarioId3.setTipo("NIF");
	    	usuarioId3.setIdentificador("4567x");
	    	objetivo.getIdentificadores().add(usuarioId3);
	    	
	    	Aplicacion aplicacion = new Aplicacion();
	    	aplicacion.setCodigo("APP1");
	    	aplicacion.setNombre("Aplicación 1");
	    	aplicacion.setAccion(accion);
	    	aplicacion.setModulo(modulo);
	    	
	    	Auditoria auditoria = new Auditoria();
	    	auditoria.setAplicacion(aplicacion);
	    	auditoria.setVariable("variableeee");
	    	auditoria.setUsuario(usuario);
	    	auditoria.setObjetivo(objetivo);
			 
	    	
	    	Gson gson = new Gson();
	    	
			String input = gson.toJson(auditoria);
			
			
	 
			Client client = Client.create();
	 
			WebResource webResource = client
			   .resource("http://localhost:8080/auditorREST/entrada");
	 
			ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, input);
	 
			if (response.getStatus() != 201) {
				throw new RuntimeException("Failed : HTTP error code : "
				     + response.getStatus());
			}
	 
			System.out.println("Output from Server .... \n");
			String output = response.getEntity(String.class);
			System.out.println(output);
	 
		  } catch (Exception e) {
	 
			e.printStackTrace();
	 
		  }
	 
	}
}
