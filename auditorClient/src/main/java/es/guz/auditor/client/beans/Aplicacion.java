package es.guz.auditor.client.beans;

public class Aplicacion {

	private String codigo;
	private String nombre;
	private Modulo modulo;
	private Accion accion;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Modulo getModulo() {
		return modulo;
	}
	public void setModulo(Modulo modulo) {
		this.modulo = modulo;
	}
	public Accion getAccion() {
		return accion;
	}
	public void setAccion(Accion accion) {
		this.accion = accion;
	}
	@Override
	public String toString() {
		return "Aplicacion [codigo=" + codigo + ", nombre=" + nombre + ", modulo=" + modulo + ", accion=" + accion
				+ "]";
	}
	
}
