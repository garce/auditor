package es.guz.auditor.client.beans;

public class Identificador {

	private String tipo;
	private String identificador;

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	@Override
	public String toString() {
		return "Identificador [tipo=" + tipo + ", identificador=" + identificador + "]";
	}

}
