package es.guz.auditor.client.beans;

public class Auditoria {

	private String ip;
	private String variable;
	private String fecha;
	private Aplicacion aplicacion;
	private Persona usuario;
	private Persona objetivo;
	public String getVariable() {
		return variable;
	}
	public void setVariable(String variable) {
		this.variable = variable;
	}
	public Aplicacion getAplicacion() {
		return aplicacion;
	}
	public void setAplicacion(Aplicacion aplicacion) {
		this.aplicacion = aplicacion;
	}
	public Persona getUsuario() {
		return usuario;
	}
	public void setUsuario(Persona usuario) {
		this.usuario = usuario;
	}
	public Persona getObjetivo() {
		return objetivo;
	}
	public void setObjetivo(Persona objetivo) {
		this.objetivo = objetivo;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	@Override
	public String toString() {
		return "Auditoria [ip=" + ip + ", variable=" + variable + ", fecha=" + fecha + ", aplicacion=" + aplicacion
				+ ", usuario=" + usuario + ", objetivo=" + objetivo + "]";
	}
	
}
