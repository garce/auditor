package es.guz.auditor;

public class AuditorException extends Exception {

	public AuditorException(String mensaje) {
		super(mensaje);
	}
	
}
