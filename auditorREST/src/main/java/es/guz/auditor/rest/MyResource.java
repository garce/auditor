package es.guz.auditor.rest;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.persist.UnitOfWork;

import es.guz.auditor.AuditorException;
import es.guz.auditor.json.AccionJ;
import es.guz.auditor.json.AplicacionJ;
import es.guz.auditor.json.AuditoriaJ;
import es.guz.auditor.json.IdentificadorJ;
import es.guz.auditor.json.ModuloJ;
import es.guz.auditor.json.PersonaJ;
import es.guz.auditor.logic.RestLogic;

@Path("entrada")
public class MyResource {
	
	private static final Logger logger = LoggerFactory.getLogger(MyResource.class);

	@Inject 
	private UnitOfWork unitOfWork;
	
	@Inject
	private RestLogic logic;

    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
	@Produces(MediaType.APPLICATION_JSON)
    public AuditoriaJ getIt() {
    	
    	ModuloJ modulo = new ModuloJ();
    	modulo.setCodigo("M1");
    	modulo.setDescripcion("Módulo 1");
    	
    	AccionJ accion = new AccionJ();
    	accion.setCodigo("AC1");
    	accion.setDescripcion("Acción 1");
    	
    	PersonaJ usuario = new PersonaJ();
    	usuario.setNombre("usuario");
    	usuario.setApellido1("usuario-ape1");
    	usuario.setApellido2("usuario-ape2");
    	IdentificadorJ usuarioId1 = new IdentificadorJ();
    	usuarioId1.setTipo("DNI");
    	usuarioId1.setIdentificador("1234");
    	usuario.getIdentificadores().add(usuarioId1);
    	IdentificadorJ usuarioId2 = new IdentificadorJ();
    	usuarioId2.setTipo("NIF");
    	usuarioId2.setIdentificador("6789");
    	usuario.getIdentificadores().add(usuarioId2);
    	
    	PersonaJ objetivo = new PersonaJ();
    	objetivo.setNombre("objetivo");
    	objetivo.setApellido1("objetivo-ape1");
    	objetivo.setApellido2("objetivo-ape2");
    	IdentificadorJ usuarioId3 = new IdentificadorJ();
    	usuarioId3.setTipo("NIF");
    	usuarioId3.setIdentificador("4567x");
    	objetivo.getIdentificadores().add(usuarioId3);
    	
    	AplicacionJ aplicacion = new AplicacionJ();
    	aplicacion.setCodigo("APP1");
    	aplicacion.setNombre("Aplicación 1");
    	aplicacion.setAccion(accion);
    	aplicacion.setModulo(modulo);
    	
    	AuditoriaJ auditoria = new AuditoriaJ();
    	auditoria.setAplicacion(aplicacion);
    	auditoria.setVariable("variableeee");
    	auditoria.setUsuario(usuario);
    	auditoria.setObjetivo(objetivo);
    	
    	return auditoria;
    }

    @POST
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String add(AuditoriaJ o, @Context final HttpServletResponse response) throws IOException {
    	// Devolvemos el id de la tupla insertada
    	try {
    		response.setStatus(response.SC_CREATED);
    		response.flushBuffer();
    		String id = Long.toString(logic.add(o));
    		logger.info("Insertado, auditoria.id = " + id);
			return id;
		} catch (AuditorException e) {
			// Salvar el error!
			String id = logic.addError(o, e.getMessage());
    		response.setStatus(response.SC_BAD_REQUEST);
    		response.flushBuffer();
    		logger.error("Error al insertar mensaje, auditoria_error.id = " + id);
			return "0";
		} catch (NoSuchAlgorithmException e) {
    		response.setStatus(response.SC_INTERNAL_SERVER_ERROR);
    		response.flushBuffer();
    		logger.error("Error con el MD5, revisa librerías");
			return "Error con el MD5, revisa librerías";
		} finally {
			// Ojo cerrar la unidad de trabajo, que sino vienen los problemas con las conexiones
			unitOfWork.end();
		}
    }
    
}
