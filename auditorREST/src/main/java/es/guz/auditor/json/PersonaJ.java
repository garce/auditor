package es.guz.auditor.json;

import java.util.ArrayList;
import java.util.List;

public class PersonaJ {

	private String nombre;
	private String apellido1;
	private String apellido2;
	private String fnac;
	private String sexo;
	private List<IdentificadorJ> identificadores;
	
	public PersonaJ() {
		this.identificadores = new ArrayList<IdentificadorJ>();
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido1() {
		return apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public List<IdentificadorJ> getIdentificadores() {
		return identificadores;
	}

	public void setIdentificadores(List<IdentificadorJ> identificadores) {
		this.identificadores = identificadores;
	}

	public String getFnac() {
		return fnac;
	}

	public void setFnac(String fnac) {
		this.fnac = fnac;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	@Override
	public String toString() {
		return "PersonaJ [nombre=" + nombre + ", apellido1=" + apellido1 + ", apellido2=" + apellido2 + ", fnac="
				+ fnac + ", sexo=" + sexo + ", identificadores=" + identificadores + "]";
	}


}
