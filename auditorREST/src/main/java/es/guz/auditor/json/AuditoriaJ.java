package es.guz.auditor.json;

public class AuditoriaJ {

	private String ip;
	private String variable;
	private String fecha;
	private AplicacionJ aplicacion;
	private PersonaJ usuario;
	private PersonaJ objetivo;
	public String getVariable() {
		return variable;
	}
	public void setVariable(String variable) {
		this.variable = variable;
	}
	public AplicacionJ getAplicacion() {
		return aplicacion;
	}
	public void setAplicacion(AplicacionJ aplicacion) {
		this.aplicacion = aplicacion;
	}
	public PersonaJ getUsuario() {
		return usuario;
	}
	public void setUsuario(PersonaJ usuario) {
		this.usuario = usuario;
	}
	public PersonaJ getObjetivo() {
		return objetivo;
	}
	public void setObjetivo(PersonaJ objetivo) {
		this.objetivo = objetivo;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	@Override
	public String toString() {
		return "AuditoriaJ [ip=" + ip + ", variable=" + variable + ", fecha=" + fecha + ", aplicacion=" + aplicacion
				+ ", usuario=" + usuario + ", objetivo=" + objetivo + "]";
	}
	
}
