package es.guz.auditor.json;

public class AplicacionJ {

	private String codigo;
	private String nombre;
	private ModuloJ modulo;
	private AccionJ accion;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public ModuloJ getModulo() {
		return modulo;
	}
	public void setModulo(ModuloJ modulo) {
		this.modulo = modulo;
	}
	public AccionJ getAccion() {
		return accion;
	}
	public void setAccion(AccionJ accion) {
		this.accion = accion;
	}
	@Override
	public String toString() {
		return "Aplicacion [codigo=" + codigo + ", nombre=" + nombre + ", modulo=" + modulo + ", accion=" + accion
				+ "]";
	}
	
}
