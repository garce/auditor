package es.guz.auditor.logic;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.google.inject.Inject;
import com.google.inject.persist.Transactional;

import es.guz.auditor.AuditorException;
import es.guz.auditor.json.AccionJ;
import es.guz.auditor.json.AplicacionJ;
import es.guz.auditor.json.AuditoriaJ;
import es.guz.auditor.json.IdentificadorJ;
import es.guz.auditor.json.ModuloJ;
import es.guz.auditor.json.PersonaJ;
import es.guz.auditor.lib.Constants;
import es.guz.auditor.lib.Utilidades;
import es.guz.auditor.lib.model.Accion;
import es.guz.auditor.lib.model.Aplicacion;
import es.guz.auditor.lib.model.Auditoria;
import es.guz.auditor.lib.model.AuditoriaError;
import es.guz.auditor.lib.model.AuditoriaInterna;
import es.guz.auditor.lib.model.Modulo;
import es.guz.auditor.lib.model.Persona;
import es.guz.auditor.lib.model.PersonaIdentificador;

public class RestLogic {

    @Inject 
    private EntityManager em; 

    @Transactional
    public long add(AuditoriaJ auditoria) throws AuditorException, NoSuchAlgorithmException {
    	
    	Auditoria nueva = new Auditoria();

		// Lo primero comprobamos que exista la APLICACIÓN
		// Si no existe, se crea, si existe se comprueba si hay que actualizar
		nueva.setAplicacion(checkAplicacion(auditoria));
		
		// Comprobamos que exista el MÓDULO
		// Si no existe, se crea, si existe se comprueba si hay que actualizar
		if (auditoria.getAplicacion().getModulo() != null) {
			nueva.setModulo(checkModulo(auditoria));
		}
		
		// Comprobamos que exista la ACCIÓN
		// Si no existe, se crea, si existe se comprueba si hay que actualizar
		if (auditoria.getAplicacion().getAccion() != null) {
			nueva.setAccion(checkAccion(auditoria));
		}
		
		// Comprobamos que exista el USUARIO (persona)
		// Si no existe, se crea, si existe se comprueba si hay que actualizar
		if (auditoria.getUsuario() != null) {
			nueva.setUsuario(checkPersona(auditoria, auditoria.getUsuario()));
		}
		
		// Comprobamos que exista el OBJETIVO (persona)
		// Si no existe, se crea, si existe se comprueba si hay que actualizar
		if (auditoria.getObjetivo() != null) {
			nueva.setObjetivo(checkPersona(auditoria, auditoria.getObjetivo()));
		}
		
		// Rellenamos los datos que faltan
		nueva.setFecha(auditoria.getFecha() != null ? new Date(Long.parseLong(auditoria.getFecha())) : null);		// Fecha que viene en el mensaje
		nueva.setFechaInsercion(new Date());																		// Fecha de cuando insertamos
		nueva.setVariable(auditoria.getVariable().getBytes());
		nueva.setMensaje(auditoria.toString().getBytes());
		nueva.setChecksum(Utilidades.getMD5(auditoria.toString()));
		nueva.setIp(auditoria.getIp());
		
		// Insertamos la tupla ya teniendo rellenados todos los datos
		em.persist(nueva);
		// Forzar los cambios para poder leer el id de la nueva tupla
		em.flush();
		
		// Devolvemos el id de fila insertada
		return Long.parseLong(nueva.getId());
    	
    } 

    private Aplicacion checkAplicacion(AuditoriaJ auditoria) throws AuditorException {
    	
    	AplicacionJ aplicacion = auditoria.getAplicacion();
    	
    	if (aplicacion != null && aplicacion.getCodigo() != null && !"".equals(aplicacion.getCodigo())) {
    	
	    	Query q = em.createNamedQuery("Aplicacion.findByCodigo", Aplicacion.class);
			q.setParameter("codigo", aplicacion.getCodigo());
			List<Aplicacion> datos = q.getResultList();
			if (datos != null && datos.size() == 1) {
				// Existe la aplicación, se comprueba si hay que actualizar el nombre
				Aplicacion app = datos.get(0);
				
				if ( aplicacion.getNombre() != null && !"".equals(aplicacion.getNombre()) && !aplicacion.getNombre().equals(app.getNombre()) ) {
					// Hay que actualizar
					
					// Cargamos el objeto proxy
					Aplicacion appProxy = em.find(Aplicacion.class, app.getId());
					// Añadimos la fila en la auditoría interna
					addInternal(Constants.OBJETIVO_APLICACION, app.getId(), Constants.ACCION_APLICACION_UPDATE, app.getNombre(), auditoria.toString());
					// Actualizamos
					appProxy.setNombre(aplicacion.getNombre());
					em.merge(appProxy);
					em.flush();
				}
				
				return app;
				
			} else if (datos.size() == 0) {
				// No existe la aplicación, se inserta
				addInternal(Constants.OBJETIVO_APLICACION, 0, Constants.ACCION_APLICACION_INSERT, null, auditoria.toString());
				
				Aplicacion app = new Aplicacion();
				app.setCodigo(aplicacion.getCodigo());
				app.setNombre(aplicacion.getNombre());
				em.persist(app);
				em.flush();
				
				return app;
			} else {
				throw new AuditorException("Existe más de una aplicación con el código " + aplicacion.getCodigo());
			}
		
    	} else {
    		throw new AuditorException("La aplicación no viene correctamente rellena");
    	}
    	
    }

    private Modulo checkModulo(AuditoriaJ auditoria) throws AuditorException {
    	
    	ModuloJ modulo = auditoria.getAplicacion().getModulo();

    	if (modulo != null && modulo.getCodigo() != null && !"".equals(modulo.getCodigo())) {
    	
	    	Query q = em.createNamedQuery("Modulo.findByAplicacionCodigo", Modulo.class);
			q.setParameter("codigoAplicacion", auditoria.getAplicacion().getCodigo());
			q.setParameter("codigoModulo", modulo.getCodigo());
			List<Modulo> datos = q.getResultList();
			if (datos != null && datos.size() == 1) {
				// Existe el módulo, se comprueba si hay que actualizar el nombre
				Modulo mod = datos.get(0);
				
				if ( modulo.getDescripcion() != null && !"".equals(modulo.getDescripcion()) && !modulo.getDescripcion().equals(mod.getDescripcion()) ) {
					// Hay que actualizar
					
					// Cargamos el objeto proxy
					Modulo modProxy = em.find(Modulo.class, mod.getId());
					// Añadimos la fila en la auditoría interna
					addInternal(Constants.OBJETIVO_MODULO, mod.getId(), Constants.ACCION_MODULO_UPDATE, mod.getDescripcion(), auditoria.toString());
					// Actualizamos
					modProxy.setDescripcion(modulo.getDescripcion());
					em.merge(modProxy);
					em.flush();
				}
				
				return mod;
				
			} else if (datos.size() == 0) {
				// No existe el módulo, se inserta
				addInternal(Constants.OBJETIVO_MODULO, 0, Constants.ACCION_MODULO_INSERT, null, auditoria.toString());
				
				// Buscamos la aplicación
		    	Query qApp = em.createNamedQuery("Aplicacion.findByCodigo", Aplicacion.class);
		    	qApp.setParameter("codigo", auditoria.getAplicacion().getCodigo());
				Aplicacion aplicacion = (Aplicacion) qApp.getSingleResult();
				
				Modulo mod = new Modulo();
				mod.setCodigo(modulo.getCodigo());
				mod.setDescripcion(modulo.getDescripcion());
				mod.setAplicacion(aplicacion);
				em.persist(mod);
				em.flush();
				
				return mod;
				
			} else {
				throw new AuditorException("Existe más de una combinación aplicación/módulo igual :: " + auditoria.getAplicacion().getCodigo() + "/" + modulo.getCodigo());
			}
    		
    	}
    	
    	return null;
    	
    }

    private Accion checkAccion(AuditoriaJ auditoria) throws AuditorException {
    	
    	AccionJ accion = auditoria.getAplicacion().getAccion();
    	
    	if (accion != null && accion.getCodigo() != null && !"".equals(accion.getCodigo())) {

	    	Query q = em.createNamedQuery("Accion.findByAplicacionCodigo", Accion.class);
			q.setParameter("codigoAplicacion", auditoria.getAplicacion().getCodigo());
			q.setParameter("codigoModulo", accion.getCodigo());
			List<Accion> datos = q.getResultList();
			if (datos != null && datos.size() == 1) {
				// Existe la acción, se comprueba si hay que actualizar el nombre
				Accion acc = datos.get(0);
				
				if ( accion.getDescripcion() != null && !"".equals(accion.getDescripcion()) && !accion.getDescripcion().equals(acc.getDescripcion()) ) {
					// Hay que actualizar
					
					// Cargamos el objeto proxy
					Accion accProxy = em.find(Accion.class, acc.getId());
					// Añadimos la fila en la auditoría interna
					addInternal(Constants.OBJETIVO_ACCION, acc.getId(), Constants.ACCION_ACCION_UPDATE, acc.getDescripcion(), auditoria.toString());
					// Actualizamos
					accProxy.setDescripcion(accion.getDescripcion());
					em.merge(accProxy);
					em.flush();
					
				}
				
				return acc;
				
			} else if (datos.size() == 0) {
				// No existe la acción, se inserta
				addInternal(Constants.OBJETIVO_ACCION, 0, Constants.ACCION_ACCION_INSERT, null, auditoria.toString());
				
				// Buscamos la aplicación
		    	Query qApp = em.createNamedQuery("Aplicacion.findByCodigo", Aplicacion.class);
		    	qApp.setParameter("codigo", auditoria.getAplicacion().getCodigo());
				Aplicacion aplicacion = (Aplicacion) qApp.getSingleResult();
				
				Accion acc = new Accion();
				acc.setCodigo(accion.getCodigo());
				acc.setDescripcion(accion.getDescripcion());
				acc.setAplicacion(aplicacion);
				em.persist(acc);
				em.flush();
				
				return acc;
				
			} else {
				throw new AuditorException("Existe más de una combinación aplicación/acción igual :: " + auditoria.getAplicacion().getCodigo() + "/" + accion.getCodigo());
			}
    		
    	}
    	
    	return null;
    }
    
    /**
     * Método para comprobar si una persona existe, hay que crearla o actualizarla.
     * Así también se comprobarán sus identificadores.
     * Si una persona se ha dado de baja y se vuelve a recibir, se dará de alta.
     * 
     * @param auditoria Necesario para poder guardar en BD el mensaje.
     * @param persona Persona que se quiere comprobar, puede ser usuario u objetivo.
     */
    private Persona checkPersona(AuditoriaJ auditoria, PersonaJ persona) throws AuditorException {
    	
    	if (persona.getIdentificadores() != null && persona.getIdentificadores().size() > 0) {
    	
    		if (persona.getIdentificadores().size() == 1) {

    			IdentificadorJ identificador = persona.getIdentificadores().get(0);
    			
    	    	Query q = em.createNamedQuery("PersonaIdentificador.findByTipoIdentificador", PersonaIdentificador.class);
    			q.setParameter("tipo", identificador.getTipo());
    			q.setParameter("identificador", identificador.getIdentificador());
    			List<PersonaIdentificador> datos = q.getResultList();
    			if (datos != null && datos.size() == 0) {
    				// Crear
    				// Guardar la persona
    				Persona p = new Persona();
    				p.setNombre(persona.getNombre());
    				p.setApellido1(persona.getApellido1());
    				p.setApellido2(persona.getApellido2());
    				p.setFnac(persona.getFnac());
    				p.setSexo(persona.getSexo());
    				addInternal(Constants.OBJETIVO_PERSONA, 0, Constants.ACCION_PERSONA_INSERT, p.toString(), auditoria.toString());
    				em.persist(p);
    				// Guardar el identificador
    				PersonaIdentificador pi = new PersonaIdentificador();
    				pi.setTipo(identificador.getTipo());
    				pi.setIdentificador(identificador.getIdentificador());
    				pi.setPersona(p);
    				em.persist(pi);
					em.flush();
					return p;
    			} else if (datos != null && datos.size() == 1) {
    				// Ya tenemos la persona!
    				
    				// Actualizamos datos
    				Persona p = datos.get(0).getPersona();
    				// Añadir auditoria interna
    				addInternal(Constants.OBJETIVO_PERSONA, p.getId().longValue(), Constants.ACCION_PERSONA_UPDATE, p.toString(), auditoria.toString());
    				p.setNombre(persona.getNombre());
    				p.setApellido1(persona.getApellido1());
    				p.setApellido2(persona.getApellido2());
    				p.setFnac(persona.getFnac());
    				p.setSexo(persona.getSexo());
    				em.merge(p);
    				em.flush();
    				
    				return datos.get(0).getPersona();
    			} else if (datos != null && datos.size() > 1) {
    				// ERROR, no puede haber más de una coincidencia tipo / identificador
    				throw new AuditorException("Se ha encontrado más de una tupla tipo/identificador :: " + identificador.getTipo() + "/" + identificador.getIdentificador());
    			}
    			
    		} else {
    		
    			// Inicializaciones
    			Persona p = null;
    			List<IdentificadorJ> identificadoresInsertar = new ArrayList<IdentificadorJ>();
    	    	Query q = em.createNamedQuery("PersonaIdentificador.findByTipoIdentificador", PersonaIdentificador.class);
    			
    			for (IdentificadorJ identificador : persona.getIdentificadores()) {
    				
    				// Buscar
        			q.setParameter("tipo", identificador.getTipo());
        			q.setParameter("identificador", identificador.getIdentificador());
        			List<PersonaIdentificador> datos = q.getResultList();
    				
    				// Si existe, asignar la persona, si ya está asignada y es diferente lanzar excepción
        			if (datos != null && datos.size() == 0) {
        				// No tiene persona, hay que guardarlo para insertar
        				identificadoresInsertar.add(identificador);
        			} else if (datos != null && datos.size() == 1) {
        				// El identificador ya existe, comprobar a quién está asignado
        				if (p == null){
        					// Todavía no habíamos encontrado a la persona asignada, se asigna y se sigue
        					p = datos.get(0).getPersona();
        				} else {
        					// Ya hay persona encontrada con otro identificador, comprobar si coinciden
        					if (p.getId() != datos.get(0).getPersona().getId()) {
        						// Como no coinciden los identificadores, lanzar excepción
                				throw new AuditorException("Se ha encontrado una colisión de identificadores, imposible insertar");
        					}
        				}
        			} else if (datos != null && datos.size() > 1) {
        				// ERROR, no puede haber más de una coincidencia tipo / identificador
        				throw new AuditorException("Se ha encontrado más de una tupla tipo/identificador :: " + identificador.getTipo() + "/" + identificador.getIdentificador());
        			}
    				
    			}
    			
    			if (p == null) {
    				
    				// Crear
    				p = new Persona();
    				p.setNombre(persona.getNombre());
    				p.setApellido1(persona.getApellido1());
    				p.setApellido2(persona.getApellido2());
    				p.setFnac(persona.getFnac());
    				p.setSexo(persona.getSexo());
    				addInternal(Constants.OBJETIVO_PERSONA, 0, Constants.ACCION_PERSONA_INSERT, p.toString(), auditoria.toString());
    				em.persist(p);
    				
    			} else {
    				
    				// Actualizar
    				// Añadir auditoria interna
    				addInternal(Constants.OBJETIVO_PERSONA, p.getId().longValue(), Constants.ACCION_PERSONA_UPDATE, p.toString(), auditoria.toString());
    				p.setNombre(persona.getNombre());
    				p.setApellido1(persona.getApellido1());
    				p.setApellido2(persona.getApellido2());
    				p.setFnac(persona.getFnac());
    				p.setSexo(persona.getSexo());
    				em.merge(p);
    				em.flush();
    				
    			}
    			
    			// Añadir los identificadores si hiciera falta
    			for (IdentificadorJ identificador : identificadoresInsertar) {
    				PersonaIdentificador pi = new PersonaIdentificador();
    				pi.setTipo(identificador.getTipo());
    				pi.setIdentificador(identificador.getIdentificador());
    				pi.setPersona(p);
    				em.persist(pi);
    			}
				em.flush();
    			
    			return p;
    			
    		}
    		
    	}
    	
    	return null;
    	
    }
    
    /**
     * 
     * @param objetivo Objetivo de la auditoria interna, Aplicación, Módulo, Acción, Persona, o Identificador Persona
     * @param idObjetivo Id de la tupla objetivo
     * @param accion Acción efectuada, inserción, modificación, eliminación
     * @param valorAnterior Valor antes de la modificación
     * @param mensaje Mensaje original que produce la modificación
     */
    private void addInternal(String objetivo, long idObjetivo, String accion, String valorAnterior, String mensaje) {
    	
    	AuditoriaInterna interna = new AuditoriaInterna();
    	interna.setObjetivo(objetivo);
    	interna.setIdObjetivo(BigInteger.valueOf(idObjetivo));
    	interna.setAccion(accion);
    	interna.setValorAnterior(valorAnterior);
    	interna.setMensaje(mensaje.getBytes());
    	
    	em.persist(interna);
		em.flush();
    	
    }
    
    /**
     * Método para guardar los errores, ya que no se insertan en auditoria
     * 
     * @param auditoria
     * @param descripcion
     */
    @Transactional
    public String addError(AuditoriaJ auditoria, String descripcion) {
    	
    	AuditoriaError error = new AuditoriaError();
    	error.setError(descripcion);
    	error.setFecha(new Date());
    	error.setMensaje(auditoria.toString().getBytes());
    	
    	// Buscamos la aplicación
    	if (auditoria != null && auditoria.getAplicacion() != null) {
	    	Query q = em.createNamedQuery("Aplicacion.findByCodigo", Aplicacion.class);
			q.setParameter("codigo", auditoria.getAplicacion().getCodigo());
			List<Aplicacion> datos = q.getResultList();
			if (datos != null && datos.size() == 1) {
				error.setAplicacion(datos.get(0));
			}
    	}
    	
    	em.persist(error);
		em.flush();
		
		return error.getId();
    	
    }
    
}
