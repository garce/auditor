package es.guz.auditor.config;

import java.util.HashMap;
import java.util.Map;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.persist.jpa.JpaPersistModule;
import com.google.inject.servlet.GuiceServletContextListener;
import com.google.inject.servlet.ServletModule;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

import es.guz.auditor.lib.Constants;



public class ApplicationSetup extends GuiceServletContextListener  {

    @Override
    protected Injector getInjector() {

        Injector injector = Guice.createInjector(new ServletModule() {

            @Override
            protected void configureServlets() {

                super.configureServlets();
                
                // Parametros para Jersey
                Map<String, String> params = new HashMap<String, String>();
                params.put("com.sun.jersey.api.json.POJOMappingFeature", "true");
                
                // Configuring Jersey via Guice:
                ResourceConfig resourceConfig = new PackagesResourceConfig("es/guz/auditor/rest");
                for (Class<?> resource : resourceConfig.getClasses()) {
                    bind(resource);
                }
                serve("/*").with(GuiceContainer.class, params);
            }
        }, new JpaPersistModule(Constants.PERSISTENCE_UNIT)); // <-- Adding other Guice Dependency Injection Modules
        
        // Inicializamos la conexión a base de datos
        injector.getInstance(Initializer.class);
        
        return injector;
        
    }
}
