package es.guz.auditor.config;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.PersistService;


@Singleton
public class Initializer {
	
    @Inject Initializer(PersistService service) {
    	service.start(); 

        // At this point JPA is started and ready.
    } 
    
}
