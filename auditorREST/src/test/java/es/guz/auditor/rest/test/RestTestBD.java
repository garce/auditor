package es.guz.auditor.rest.test;
import java.security.NoSuchAlgorithmException;

import org.jukito.JukitoModule;
import org.jukito.JukitoRunner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.google.inject.persist.jpa.JpaPersistModule;

import es.guz.auditor.AuditorException;
import es.guz.auditor.config.Initializer;
import es.guz.auditor.json.AccionJ;
import es.guz.auditor.json.AplicacionJ;
import es.guz.auditor.json.AuditoriaJ;
import es.guz.auditor.json.IdentificadorJ;
import es.guz.auditor.json.ModuloJ;
import es.guz.auditor.json.PersonaJ;
import es.guz.auditor.lib.Constants;
import es.guz.auditor.logic.RestLogic;

/**
 * @author guzman
 * 
 * Test que comprueba que los métodos funcionan contra base de datos
 *
 */
@RunWith(JukitoRunner.class)
public class RestTestBD {

	@Inject Initializer initializer;	// Inicializar la conexión
	
	@Inject RestLogic logic;
	
	public static class Module extends JukitoModule {
		protected void configureTest() {

			install(new JpaPersistModule(Constants.PERSISTENCE_UNIT));
			
		}
	}

	
	@Test
	public void testBD() {
		testSimple();
		testModulos();
		testAcciones();
		testCompletoSinDescripciones();
		testColisionIdentificadores();
	}
	
	public void testSimple() {
    	
		try {
			
	    	AplicacionJ aplicacion = new AplicacionJ();
	    	aplicacion.setCodigo("APP1");
	    	aplicacion.setNombre("Aplicación 1");
	
	    	PersonaJ usuario = new PersonaJ();
	    	usuario.setNombre("usuario");
	    	usuario.setApellido1("usuario-ape1");
	    	usuario.setApellido2("usuario-ape2");
	    	IdentificadorJ usuarioId1 = new IdentificadorJ();
	    	usuarioId1.setTipo("DNI");
	    	usuarioId1.setIdentificador("1234");
	    	usuario.getIdentificadores().add(usuarioId1);
	
	    	AuditoriaJ simple = new AuditoriaJ();
	    	simple.setAplicacion(aplicacion);
	    	simple.setVariable("variableeee");
	    	simple.setUsuario(usuario);
			
			// Comprobamos que inserta correctamente
			Assert.assertTrue(logic.add(simple) > 0);
			
		} catch (NoSuchAlgorithmException | AuditorException e) {
			System.out.println(e.getMessage());
		}
		
	}

	public void testModulos() {

		try {

	    	ModuloJ modulo = new ModuloJ();
	    	modulo.setCodigo("M1");
	    	modulo.setDescripcion("Módulo 1");
			
	    	AplicacionJ aplicacion = new AplicacionJ();
	    	aplicacion.setCodigo("APP1");
	    	aplicacion.setNombre("Aplicación 1");
	    	aplicacion.setModulo(modulo);
	
	    	PersonaJ usuario = new PersonaJ();
	    	usuario.setNombre("usuario");
	    	usuario.setApellido1("usuario-ape1");
	    	usuario.setApellido2("usuario-ape2");
	    	IdentificadorJ usuarioId1 = new IdentificadorJ();
	    	usuarioId1.setTipo("DNI");
	    	usuarioId1.setIdentificador("1234");
	    	usuario.getIdentificadores().add(usuarioId1);
	
	    	AuditoriaJ simple = new AuditoriaJ();
	    	simple.setAplicacion(aplicacion);
	    	simple.setVariable("variableeee");
	    	simple.setUsuario(usuario);
			
			// Comprobamos que inserta correctamente
			Assert.assertTrue(logic.add(simple) > 0);
			
			// Modificamos el módulo
			modulo.setDescripcion("Módulo 10");
			Assert.assertTrue(logic.add(simple) > 0);
			
		} catch (NoSuchAlgorithmException | AuditorException e) {
			System.out.println(e.getMessage());
		}
		
	}
	
	public void testAcciones() {

		try {
	    	
	    	AccionJ accion = new AccionJ();
	    	accion.setCodigo("AC1");
	    	accion.setDescripcion("Acción 1");
			
	    	AplicacionJ aplicacion = new AplicacionJ();
	    	aplicacion.setCodigo("APP1");
	    	aplicacion.setNombre("Aplicación 1");
	    	aplicacion.setAccion(accion);
	
	    	PersonaJ usuario = new PersonaJ();
	    	usuario.setNombre("usuario");
	    	usuario.setApellido1("usuario-ape1");
	    	usuario.setApellido2("usuario-ape2");
	    	IdentificadorJ usuarioId1 = new IdentificadorJ();
	    	usuarioId1.setTipo("DNI");
	    	usuarioId1.setIdentificador("1234");
	    	usuario.getIdentificadores().add(usuarioId1);
	
	    	AuditoriaJ simple = new AuditoriaJ();
	    	simple.setAplicacion(aplicacion);
	    	simple.setVariable("variableeee");
	    	simple.setUsuario(usuario);
			
			// Comprobamos que inserta correctamente
			Assert.assertTrue(logic.add(simple) > 0);
			
			// Modificamos el módulo
			accion.setDescripcion("Acción 10");
			Assert.assertTrue(logic.add(simple) > 0);
			
		} catch (NoSuchAlgorithmException | AuditorException e) {
			System.out.println(e.getMessage());
		}
		
	}
	
	public void testCompletoSinDescripciones() {

		try {

	    	ModuloJ modulo = new ModuloJ();
	    	modulo.setCodigo("M1");
	    	
	    	AccionJ accion = new AccionJ();
	    	accion.setCodigo("AC1");
			
	    	AplicacionJ aplicacion = new AplicacionJ();
	    	aplicacion.setCodigo("APP1");
	    	aplicacion.setAccion(accion);
	    	aplicacion.setModulo(modulo);
	
	    	PersonaJ usuario = new PersonaJ();
	    	IdentificadorJ usuarioId1 = new IdentificadorJ();
	    	usuarioId1.setTipo("DNI");
	    	usuarioId1.setIdentificador("1234");
	    	usuario.getIdentificadores().add(usuarioId1);
	
	    	PersonaJ objetivo = new PersonaJ();
	    	objetivo.setNombre("objetivo");
	    	objetivo.setApellido1("objetivo-ape1");
	    	objetivo.setApellido2("objetivo-ape2");
	    	IdentificadorJ objetivoId1 = new IdentificadorJ();
	    	objetivoId1.setTipo("DNI");
	    	objetivoId1.setIdentificador("54612X");
	    	objetivo.getIdentificadores().add(objetivoId1);
	    	IdentificadorJ objetivoId2 = new IdentificadorJ();
	    	objetivoId2.setTipo("NHC");
	    	objetivoId2.setIdentificador("14");
	    	objetivo.getIdentificadores().add(objetivoId2);
	
	    	AuditoriaJ simple = new AuditoriaJ();
	    	simple.setAplicacion(aplicacion);
	    	simple.setVariable("variableeee");
	    	simple.setUsuario(usuario);
	    	simple.setUsuario(objetivo);
			
			// Comprobamos que inserta correctamente
			Assert.assertTrue(logic.add(simple) > 0);
			
		} catch (NoSuchAlgorithmException | AuditorException e) {
			System.out.println(e.getMessage());
		}
		
	}
	
	public void testColisionIdentificadores() {

		long l = 0;
		try {
			
	    	AplicacionJ aplicacion = new AplicacionJ();
	    	aplicacion.setCodigo("APP1");
	
	    	PersonaJ usuario = new PersonaJ();
	    	IdentificadorJ usuarioId1 = new IdentificadorJ();
	    	usuarioId1.setTipo("DNI");
	    	usuarioId1.setIdentificador("1234");
	    	usuario.getIdentificadores().add(usuarioId1);
	
	    	PersonaJ objetivo = new PersonaJ();
	    	objetivo.setNombre("objetivo");
	    	objetivo.setApellido1("objetivo-ape1");
	    	objetivo.setApellido2("objetivo-ape2");
	    	IdentificadorJ objetivoId1 = new IdentificadorJ();
	    	objetivoId1.setTipo("DNI");
	    	objetivoId1.setIdentificador("54612X");
	    	objetivo.getIdentificadores().add(objetivoId1);
	    	objetivo.getIdentificadores().add(usuarioId1);
	
	    	AuditoriaJ simple = new AuditoriaJ();
	    	simple.setAplicacion(aplicacion);
	    	simple.setVariable("variableeee");
	    	simple.setUsuario(usuario);
	    	simple.setUsuario(objetivo);
			
			// Comprobamos que inserta correctamente
	    	l = logic.add(simple);
			
		} catch (NoSuchAlgorithmException | AuditorException e) {
			System.out.println(e.getMessage());
		}
		
		Assert.assertTrue(l == 0);
		
	}

}
